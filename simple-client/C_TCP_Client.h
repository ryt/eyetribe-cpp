#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef _WINSOCK2_H_
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <MSWSock.h>
// link with Ws2_32.lib
#pragma comment(lib, "Ws2_32.lib")
#endif

#include <iostream>
#include <string>

class C_TCP_Client
{
private:
	SOCKET tcp_sock;
	struct sockaddr_in server_info;
	int _dest_Port;

public:

	C_TCP_Client(void)
	{
		WSADATA wsaData;
		// Initialize Winsock
		int ires = WSAStartup(MAKEWORD(2,2), &wsaData);
		if(ires != 0)
		{printf("WSAStartup failed (TCP): %d\n", ires);}
		tcp_sock=NULL;
	}

	~C_TCP_Client(void){}

	/// setup TCP socket
	/// return false if failed
	bool C_TCP_Client::setup(std::string server_address, int dest_Port)
	{
		struct addrinfo	*result = NULL, hints;
		char temp[20];

		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;
		hints.ai_flags = AI_PASSIVE;

		_itoa(dest_Port+30,temp,10);
		_dest_Port = dest_Port;

		int ires = getaddrinfo(NULL, temp, &hints, &result);
		if (ires != 0)
		{printf("getaddrinfo failed (TCP): %d\n", ires); WSACleanup(); return false;}

		tcp_sock = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
		if(tcp_sock==INVALID_SOCKET)
		{
			std::cout << "Assign TCP socket failed -- " << dest_Port << std::endl;
			return false;
		}

		server_info.sin_family = AF_INET;
		server_info.sin_port = htons(dest_Port);
		server_info.sin_addr.S_un.S_addr = inet_addr(server_address.c_str());
		return true;
	}

	/// attempt to connect to server
	/// return true if succesful, false if failed
	bool C_TCP_Client::try_connect(int k=10)
	{
		u_long NonBlock=1;
		int n=k;

		while(n-->0)
		{
			if(connect(tcp_sock,(sockaddr*)&server_info,sizeof(server_info)) != SOCKET_ERROR)
			{
				std::cout << "Connected to = " << _dest_Port << " (TCP ASYNC) \n";
				ioctlsocket(tcp_sock,FIONBIO,&NonBlock);
				return true;
			}
		}
		std::cout << "Connection attempt to = " << _dest_Port << " failed (TCP) \n";
		return false;
	}

	/// closing connection
	bool C_TCP_Client::close()
	{
		closesocket(tcp_sock);
		return true;
	}

	/// Final Cleaning
	/// only use at the end of application -- WSACleanup
	bool C_TCP_Client::finish()
	{
		WSACleanup();
		return true;
	}

	/// send data over TCP
	bool C_TCP_Client::send_socket(std::string msg)
	{
		send(tcp_sock,msg.c_str(),msg.length(),0);
		return true;
	}

	bool C_TCP_Client::send_socket(char *data, int len)
	{
		send(tcp_sock,data,len,0);
		return true;
	}

	/// read data from non-blocking TCP - stored in string
	/// no data return 0, disconnected return -1, else return number of received bytes
	int C_TCP_Client::read_socket(std::string *stor)
	{
		int recvsize;
		int error_type;
		char temp[4096];

		recvsize = recv(tcp_sock,temp,4096,0);
		if(recvsize == SOCKET_ERROR)
		{
			error_type = WSAGetLastError();
			if(error_type == WSAEWOULDBLOCK) return 0;
			else if(error_type == WSAECONNRESET || error_type == WSAECONNABORTED) return -1;
		}
		else
		{
			stor->assign(temp,recvsize);
		}
		return recvsize;
	}

	/// read data from non-blocking TCP - stored in array of char
	/// no data return 0, disconnected return -1, else return number of received bytes
	int C_TCP_Client::read_socket(char* stor, const int storlen)
	{
		int recvsize;
		int error_type;

		recvsize = recv(tcp_sock,stor,storlen,0);
		if(recvsize == SOCKET_ERROR)
		{
			error_type = WSAGetLastError();
			if(error_type == WSAEWOULDBLOCK) return 0;
			else if(error_type == WSAECONNRESET || error_type == WSAECONNABORTED) return -1;
		}
		return recvsize;
	}
};
