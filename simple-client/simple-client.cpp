// simple-client.cpp : Defines the entry point for the console application.
//
#include <conio.h>
#include "eyetribe_client.h"

eyetribe_client client;

int main(int argc, char* argv[])
{
	int ret;
	client.init("127.0.0.1",6555,220);
	while(!_kbhit())
	{
		if(client.get_eye_coordinate())
		{
			std::cout << client.eye_L.state << " ; " << client.eye_L.x << " ; " << client.eye_L.y <<std::endl;
		}
		Sleep(100);
	};
	client.running_state = false;
	return 0;
}

