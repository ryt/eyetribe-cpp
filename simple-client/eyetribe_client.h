#pragma once
#include "C_TCP_Client.h"
#include "picojson.h"
#include <thread>
#include <atomic>
#include <chrono>
#include <thread>
#include <mutex>

class eyetribe_client
{
private:
	C_TCP_Client connect_mgr;
	std::string REQ_CONNECT; 
	std::string REQ_HEATBEAT;
	int heart_beat_rate;
	std::thread listener;
	std::mutex lock;
	std::string shared_buffer;

public:
	std::atomic<bool> running_state;
	struct
	{
		int x;
		int y;
		int state;
	}eye_R,eye_L;

	eyetribe_client(void)
	{
		REQ_CONNECT  = "{\"values\":{\"push\":true,\"version\":1},\"category\":\"tracker\",\"request\":\"set\"}"; 
		REQ_HEATBEAT = "{\"category\":\"heartbeat\",\"request\":null}";
		running_state=false;
	};

	~eyetribe_client(void)
	{
		listener.detach();
		connect_mgr.close();
		connect_mgr.finish();		
	}

	/// initiate connection
	int init(std::string IP_addr, int port, int hb_rate)
	{
		heart_beat_rate = (hb_rate+1)/10;
		bool state;
		///setup socket
		state = connect_mgr.setup(IP_addr,port);
		if(!state) return -1;
		state = connect_mgr.try_connect();
		if(!state) return -2;
		///request connection
		connect_mgr.send_socket(REQ_CONNECT);
		//create_listener thread
		listener = std::thread(&eyetribe_client::listen_data,this);
		//sleep to ensure new thread has been started
		Sleep(5);
		if(!running_state) return -3;
		return 0;
	}

	/// grab data
	bool get_eye_coordinate()
	{
		std::string local;
		int cnt=3;
		int ret_val;
		while(cnt-->0)
		{
			if(lock.try_lock())
			{
				local.assign(shared_buffer);
				lock.unlock();
				ret_val = parse_data(local);
				if(ret_val==200) return true;
				else return false;
			}
		}
		return false;
	}

private:
	/// listener_thread
	void listen_data()
	{
		int iterate=0;
		std::string recv;
		std::chrono::milliseconds span(1);
		running_state = true;
		///send first heartbeat
		connect_mgr.send_socket(REQ_HEATBEAT);
		///listener loop
		while (running_state.load()==true)
		{
			/// check received tcp packet
			recv.clear();
			connect_mgr.read_socket(&recv);
			if(recv.length() > 0)
			{
				///try to copy
				if(lock.try_lock())
				{
					shared_buffer.assign(recv);
					lock.unlock();
				}
			}
			//check if required to send HB
			iterate++;
			if(iterate>heart_beat_rate)
			{
				iterate = 0;
				connect_mgr.send_socket(REQ_HEATBEAT);
			}
			Sleep(10);
		};
	}

	////data parsing
	int parse_data(std::string &s)
	{
		picojson::value v;
		std::string err = picojson::parse(v, s.c_str());

		int status = (int) v.get("statuscode").get<double>();
		///return failure code
		if(status!=200) return status;

		bool no_values = v.get("values").is<picojson::null>();
		if(no_values) return -1;

		int state = (int) v.get("values").get("frame").get("state").get<double>();
		state = (state & 0x01); 
		eye_R.state = eye_L.state = state;

		picojson::value left  = v.get("values").get("frame").get("lefteye").get("avg");
		picojson::value right = v.get("values").get("frame").get("lefteye").get("avg");

		eye_L.x = (int) left.get("x").get<double>();
		eye_L.y = (int) left.get("y").get<double>();
		eye_R.x = (int) right.get("x").get<double>();
		eye_R.y = (int) right.get("y").get<double>();

		return status;
	}

};

